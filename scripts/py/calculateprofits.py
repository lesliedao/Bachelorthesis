import pickle
import csv
import math
import sys

readfile = "pickleoutput" + sys.argv[1] + ".txt"
outfile = "profits" + sys.argv[1] + ".txt"

def P(Q):
    price = 400 - 0.8 * Q
    return price if price > 0 else 0

def c(q):
    return 4 * q

endprofits = []
with open(readfile, "rb") as f:
    endstates = pickle.load(f)
    for state in endstates:
        profits = []
        price = P(sum(state))
        for i in state:
            profits.append(math.ceil((price * i - c(i)) * 100) / 100)
        endprofits.append(profits)

with open(outfile, "w") as f:
    writer = csv.writer(f)
    for profit in endprofits:
        writer.writerow(profit)
