# pylint: disable=C0103,C0111,C0303,C0301
##
# Mixed composition
# Leslie Dao
# 10561234
#
# A script for simulating the mixed composition model: N firms following
# two rules.
# s firms follow the BEST PLAYER RULE (average)
# N - s firms follow the BEST ACTION RULE (average)
##

# MODULES
import math
import random
import csv
import pickle
import timeit
import json

# Keep track of runtime
timerstart = timeit.default_timer()

# CONSTANTS
N = 5
K = 3
epsilon = 0.01
s = 4

Gamma = []
Gammamin = 80.0
Gammamax = 102
delta = 0.25
i = Gammamin
while i <= Gammamax:
    Gamma.append(i)
    i += delta

# FUNCTIONS
# Note: q^C = 82.5 and q^W = 99

# Inverse demand function
def P(q):
    p = 400 - 0.8 * q
    # Only return p if it's not negative
    if p >= 0:
        return p
    else:
        return 0

# Cost function (for all firms)
def cost(q):
    return 4 * q

# Returns the quantity that should be played according to the followed rule
def imitate(rule):
    if rule == "ba":
        # Best action rule
        # Determine which strategies have the highest average profit in memory
        highestStrategyProfit = max(strategyProfits.items(), key=lambda x: x[1]["average"])[1]["average"]
        bestStrategies = [strategyPair for strategyPair in strategyProfits.items() if strategyPair[1]["average"] == highestStrategyProfit]

        # If multiple strategies, pick random
        return random.choice(bestStrategies)[0]
    else:
        # Best player rule
        # Determine which firms have the highest average profit in memory
        highestFirmProfit = max(firms, key=lambda x: x["avgprofit"])["avgprofit"]
        bestFirms = [firmDict for firmDict in firms if firmDict["avgprofit"] == highestFirmProfit]

        # If multiple firms, pick random and return what they played in the previous period
        return random.choice(bestFirms)["strategies"][0]

# Check if the elements in an array are all the same
def allSame(array):
    return all(x == array[0] for x in array)

# Function to save output to file
def save_output(f, array):
    writer = csv.writer(f)
    for row in array:
        writer.writerow(row)


# SIMULATION
nSimulations = 1000
T = 10000
endstates = []
histogram = {"allsame": 0, "s/N-s": 0}
for i in xrange(nSimulations):
    print "Running simulation %d..." % (i + 1)
    # INITIALIZE this for every simulation: do not use history from past simulations
    # Make a dictionary to keep track of the profit history and profit average for each strategy
    strategyProfits = {}
    i = Gammamin
    while i <= Gammamax:
        # Initial average profit is 0
        strategyProfits[i] = {"history": [], "average": 0}
        i += delta

    # List of firms, containing their past strategies, profits, average profit and the rule they're following
    firms = [{"rule": "ba", "strategies": [], "profits": [], "avgprofit": 0} for x in range(N)]
    # Set s firms to the best player rule
    for i in xrange(s):
        firms[i]["rule"] = "bp"

    # Append to front by using array.insert(0, var) and pop the last element: array.pop()

    period = 0
    history = []
    while period < T:
        period += 1
        Q = 0
        nextStrategies = []

        for firm in firms:
            # Use Unif(0, 1) to determine whether to imitate or experiment, always experiment in the first period
            if random.random() < epsilon or period == 1:
                # Experiment
                nextq = random.choice(Gamma)
            else:
                # Imitate
                nextq = imitate(firm["rule"])

            # It's important not to append the strategy to the firms yet, as they take the decision simultaneously
            nextStrategies.append(nextq)
            Q += nextq

        # Calculate price with inverse demand function
        price = P(Q)

        # Keep track of used and unused strategies for updating their average profit
        strategiesUsed = []

        for k in xrange(len(firms)):
            # Append strategies to start of memory for each firm
            firms[k]["strategies"].insert(0, nextStrategies[k])

            # Calculate profits for each firm
            firms[k]["profits"].insert(0, math.ceil((price * nextStrategies[k] - cost(nextStrategies[k])) * 100) / 100)

            # Only add unique elements to strategiesUsed
            if nextStrategies[k] not in strategiesUsed:
                strategiesUsed.append(nextStrategies[k])

            # Remove strategies and profits outside of memory, current period + K previous periods for K + 1 periods
            if len(firms[k]["strategies"]) > K + 1:
                firms[k]["strategies"].pop()

            if len(firms[k]["profits"]) > K + 1:
                firms[k]["profits"].pop()

            # Update the average profit for each firm
            firms[k]["avgprofit"] = math.ceil(100 * sum(firms[k]["profits"], 0.0) / len(firms[k]["profits"])) / 100

        # Update the strategyProfits dictionary for the strategies
        for strategy in Gamma:
            # If the strategy was USED
            if strategy in strategiesUsed:
                # Append the profit of the used strategy to the beginning of the history list
                strategyProfits[strategy]["history"].insert(0, math.ceil((price * strategy - cost(strategy)) * 100) / 100)
            # Else, the strategy was NOT USED
            else:
                # Append "NP" (not played) to the beginning of the history list
                strategyProfits[strategy]["history"].insert(0, "NP")

            # If the history list is longer than the allowed memory, pop the last element off
            if len(strategyProfits[strategy]["history"]) > K + 1:
                strategyProfits[strategy]["history"].pop()

            # Update the average profit for the strategy but do not count "NP", for when the strategy was not played
            cleanHistory = [x for x in strategyProfits[strategy]["history"] if x != "NP"]

            # If the strategy was played at least once in memory
            if len(cleanHistory) != 0:
                strategyProfits[strategy]["average"] = sum(cleanHistory, 0.0) / len(cleanHistory)
            # If the strategy was NOT played in memory, change the average profit to 0
            else:
                strategyProfits[strategy]["average"] = 0

        history.insert(0, nextStrategies)
        if len(history) > K + 1:
            history.pop()

        # Optional break statement
        # TODO: add additional note if the exit state was monomorphic
        # if len(history) == K + 1 and allSame(history) and allSame(history[0]):
        #     break

    # Save simulation: save nextStrategies as that is the state
    endstates.append(nextStrategies)
    if allSame(nextStrategies):
        histogram["allsame"] += 1
    else:
        histogram["s/N-s"] += 1

# TODO: histogram of end states for analysing distribution
# Write to file
with open("output.txt", "wb") as outputf:
    save_output(outputf, endstates)
    json.dump(histogram, outputf, indent = 2)

# Pickle to file
with open("pickleoutput.txt", "wb") as picklef:
    pickle.dump(endstates, picklef)

# Print runtime
timerstop = timeit.default_timer()
print "Runtime: %.5f seconds" % (timerstop - timerstart)

# TESTS
