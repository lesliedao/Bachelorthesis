var n = 50;
var uniformGenerator = function() {
    return Math.random();
};
var data = d3.range(n).map(function() {return 0;});
var sum = 0;

var margin = {top: 20, bottom: 20, left: 40, right: 30};
var width = 800 - margin.left - margin.right;
var height = 300 - margin.top - margin.bottom;

var x = d3.scale.linear()
    .domain([1, n - 2])
    .range([0, width]);

var y = d3.scale.linear()
    .domain([0, 1])
    .range([height, 0]);

var line = d3.svg.line()
    .interpolate("basis")
    .x(function(d, i) {return x(i);})
    .y(function(d, i) {return y(d);});

var svg = d3.select("#path1").append("svg")
    .attr("class", "graph")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.append("defs").append("clipPath")
    .attr("id", "clip")
    .append("rect")
        .attr("width", width)
        .attr("height", height);

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.svg.axis().scale(x).orient("bottom"));

svg.append("g")
    .attr("class", "y axis")
    .call(d3.svg.axis().scale(y).ticks(5).orient("left"));


var path = svg.append("g")
    .attr("clip-path", "url(#clip)")
    .append("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line);

var text = d3.select("#path1").append("p")
    .text(sum);

tick();

function tick() {
    var newPoint = uniformGenerator();
    data.push(newPoint);
    sum += newPoint;
    text.text(sum.toFixed(3));

    path.attr("d", line)
        .attr("transform", null)
        .transition()
            .duration(400)
            .ease("linear")
            .attr("transform", "translate(" + x(0) + ",0)")
            .each("end", tick);

    data.shift();
}
